// Trent Begin
// Project Euler - Problem 3

/**
The prime factors of 13195 are 5, 7, 13 and 29.

What is the largest prime factor of the number 600851475143 ?
**/

#include <iostream>

static const unsigned long NUMBER = 600851475143;

int main(int argc, char** argv) {

    std::cout << "Hallo World!" << std::endl;
    
    unsigned long i = 2;
    unsigned long prime = 1;
    unsigned long num = 0;

    bool primeFound = true;

    while(num <= NUMBER / 2) {
        
        //std::cout << "i: " << i << std::endl;
        primeFound = false;
        
        int num = 6*i;
        
        if( (num - 1) % NUMBER == 0 ) {
            primeFound = true;
            prime = num - 1;
        }

        if( (num + 1) % NUMBER == 0 ) { 
            primeFound = true; 
            prime = num + 1;
        }


        std::cout << "Prime: " << prime << std::endl;
        i++;
    }

    std::cout << "Largest Prime: " << prime << std::endl;
    return 0;
}
