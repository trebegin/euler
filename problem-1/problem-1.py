#!/bin/python


# Trent Begin
# Project Euler Problem 1
# https://projecteuler.net/problem=1

def main():
    total = 0    

    for x in range(0, 1000):
        if (x % 3) == 0 or (x % 5) == 0:
            total += x
    
    print ("Total: " + str(total)) 

if __name__ == "__main__":
    main()


    
