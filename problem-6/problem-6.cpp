// Trent Begin
// Project Euler - Problem 6

/** 

The sum of the squares of the first ten natural numbers is,
12 + 22 + ... + 102 = 385

The square of the sum of the first ten natural numbers is,
(1 + 2 + ... + 10)2 = 552 = 3025

Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025 − 385 = 2640.

Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.

**/ 

#include<iostream>
#include<iomanip>
#include<cmath>

using std::cout;
using std::endl;
using std::pow;

static const int NUMBER = 10000;

int main(int argc, char** argv) {

    unsigned int total = 0;
    unsigned int sum = 0;
    
    for(unsigned int i = 1; i <= NUMBER; i++) {
        total += pow(i, 2);
        sum += i;
    }

    cout << "Answer: " << std::setprecision(20) << (pow(sum, 2) - total) << endl;
}
