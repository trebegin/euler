#include <iostream>
#include <vector>

static const int UPPER_BOUND = 4000000;

int main(int argc, char** argv) {
    
    int i = 1;
    int j = 2;
    int k = 2;

    int sum = 0;

    while(k < UPPER_BOUND) {
    
        if(k % 2 == 0) 
            sum += k; 

        i = j;
        j = k;
        k = i + j;
    }

    std::cout << "Sum: " << sum << std::endl;

    return 0;
}
