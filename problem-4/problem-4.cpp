// Trent Begin
// Project Euler - Problem 4

/**

A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.

Find the largest palindrome made from the product of two 3-digit numbers.

**/

#include <iostream>
#include <string>

using std::cout;
using std::endl;
using std::string;

static const int UPPER_BOUND = 999999;

inline bool isPalindrome(int num) {

    string numStr = std::to_string(num);
    int length = numStr.length();    
    
    int i = 0;
    int j = length - 1;

    while(i < length / 2) { 
        if (numStr.at(i) != numStr.at(j))
            return false;    
        
        i++;
        j--; 
    }  
   
    return true; 
}

int main(int argc, char** argv) {

    cout << "Hallo" << endl;

    int num = 0;

    for(int i = UPPER_BOUND; i >= 1; i--) {

        for(int j = UPPER_BOUND; j >= 1; j--) {
            
            if(i*j < num) break;
            
            if (isPalindrome(i*j)) {
                if(num < i*j) num = i*j;
            }
        }
    }  

    cout << "Largest Palindrome: " << num << endl;
}


